import "./SearchBar.css";
import React from "react";

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { searchTerm: 2019 };
  }

  onSearch = event => {
    this.setState({ searchTerm: event.target.value });
  };

  onSubmit = event => {
    event.preventDefault();
    this.props.onSubmit(this.state.searchTerm);
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <input
            className="seachbar"
            type="text"
            placeholder="Zoek hier op jaartal"
            onChange={this.onSearch}
            value={this.state.searchTerm}
            onSubmit={this.props.onSubmit}
          />
        </form>
      </div>
    );
  }
}

export default SearchBar;
