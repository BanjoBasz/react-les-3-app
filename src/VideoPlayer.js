import React from "react";

import "./VideoPlayer.css";

const VideoPlayer = props => {
  return (
    <div>
      <video className="videoPlayer" key={props.video} width="400" controls>
        <source src={props.video || ""} type="video/mp4" controls />
      </video>
    </div>
  );
};
export default VideoPlayer;
